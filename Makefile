WEB_REPOSITORY = "registry.gitlab.com/zipofar/test-ci"
VERSION ?= $$(git rev-parse HEAD)

ecr_login:
	docker login registry.gitlab.com -u zipofar@gmail.com -p ${GITLAB_ACCESS_TOKEN}

build_image:
	docker build --cache-from ${WEB_REPOSITORY}:latest -t ${WEB_REPOSITORY}:${VERSION} -t ${WEB_REPOSITORY}:latest .

push_image: ecr_login
	docker push ${WEB_REPOSITORY}:${VERSION}
	docker push ${WEB_REPOSITORY}:latest

pull_image: ecr_login
	docker pull ${WEB_REPOSITORY}:${VERSION}

export_image_names:
	export WEB_IMAGE=${WEB_REPOSITORY}
